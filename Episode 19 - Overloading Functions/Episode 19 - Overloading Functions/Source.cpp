#include <iostream>

using namespace std;

void speak(string message) {
	cout << message << endl;
}

//Overloading the function by adding different parameters
void speak(int number) {
	cout << number << endl;
}
//another overload of the same function
void speak(double number) {
	cout << number << endl;
}

//a more practical example 
double Area(double radius) { //circle
	return 3.14 * radius * radius;
}

double Area(double base, double height) { //triangle
	int area = (base * height) / 2;
	return area;
}

int main() {

	//Overloading functions - Multiple versions of the same function
	speak("Poopy pants");
	speak(45);
	speak(123.1);

	//depending on what parameters is used, a different area is calculated
	cout << "Area of my circle " << Area(5) << endl;
	cout << "Area of my triangle " << Area(2, 3) << endl;


	return 0;
}