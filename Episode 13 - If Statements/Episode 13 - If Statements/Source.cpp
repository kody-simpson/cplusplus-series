#include <iostream>

using namespace std;

int main() {


	//If statements - control the flow of the program
	int age;
	cout << "Whats your age? ";
	cin >> age;
	cout << "Thanks." << endl;
	if (age >= 18) { //if the condition is true, the block of code is executed
		cout << "Oh wow!! You are of legal age for a beer! Here you go." << endl;
		cout << "*gives beer*" << endl;
	}
	else { //if the condition was false, this is executed
		cout << "You are not old enough for a beer! Go away!" << endl;
	}

	//another example
	int number = 3;
	if (number > 0)
		cout << "The number is positive." << endl;
	else
		cout << "The number is negative." << endl;

	//nested if statements
	string answer;
	cout << "Would you like a beer?! ";
	cin >> answer;
	if (answer == "yes")
	{ //you can have if statements inside of each other
		cout << "Whats your age? ";
		cin >> age;
		cout << "Thanks." << endl;
		if (age >= 18) {
			cout << "Oh wow!! You are of legal age for a beer! Here you go." << endl;
			cout << "*gives beer*" << endl;
		}
		else {
			cout << "You are not old enough for a beer! Go away!" << endl;
		}
	}

	//how to have multiple conditions
	int score;
	cout << "What score did you get? " << endl;
	cin >> score;
	if (score == 0)
	{
		cout << "Wow, you suck!" << endl;
	}
	else if (score >= 1000) { //check for another condition
		cout << "You beat the highscore! " << endl;
	}
	else if (score > 500) {
		cout << "Almost there!" << endl;
	}
	else
	{
		cout << "Score higher!" << endl;
	}


	return 0;
}