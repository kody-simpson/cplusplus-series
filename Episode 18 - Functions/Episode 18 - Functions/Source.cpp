#include <iostream>

using namespace std;

//function prototype
int addNumbers(int num1, int num2);
//void means nothing is returned
void saySomething(string message);

//Make the function definition before the function is called and you dont have to make a prototype
int subtractNumbers(int num1, int num2) {
	int result = num1 - num2; //instance variable
	return result;
}

//You can add parameters with default values
string makeNameMessage(string name = "Bob");

int main() {

	//Functions - an easy way to group and repeat code

	//Call a function
	addNumbers(5, 6); //equivalent to 11, since it returns the result
	cout << addNumbers(5, 6) << endl;
	cout << addNumbers(3, 4) << endl; //Call as many times as you want

	saySomething("Root beer is really good");

	cout << subtractNumbers(11, 7) << endl;

	//If no parameter is entered, the default param is used.
	cout << makeNameMessage() << endl;

	return 0;
}

//the definition for the prototype declared above
int addNumbers(int num1, int num2) {
	return num1 + num2; //Add 2 numbers and return the value
}

void saySomething(string message) {
	cout << message << endl;
}

string makeNameMessage(string name) {
	string message = "Hello, my name is " + name;
	return message;
}