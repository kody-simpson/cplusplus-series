#include <iostream> //Include the iostream file in our file

using namespace std; //Automatically use the standard library namespace

int main() { //the function that must be included in every program
	cout << "Hello World!"; //An output statement. We pass a string literal to the cout output stream

	return 0; //0 or nothing means the program ran successfully. a Non-zero value means failure.
}