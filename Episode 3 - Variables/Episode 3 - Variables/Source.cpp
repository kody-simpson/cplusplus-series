#include <iostream>

using namespace std;

int main() {
	//Variables - a portion of memory to store a value
	//naming rules - has to start with letter or underscore
	//cannot include Spaces, punctuation marks, and symbols

	//Fundamental Variable Types
	//Character variables - used for storing characters(letters and symbols)
	//Numerical Integer Types - Non-decimal numbers
	//Floating-point numbers - Decimal numbers
	//Booleans - True or False

	//Declare variables
	int bob = 5;
	bob = 10; //You can re-assign the values

	int sherry; //make variable, assign later
	sherry = -2000;

	//Print out variables
	cout << bob << endl;
	cout << sherry << endl;

	//Declare multiple varialbes at once
	int a, b, c; //Creates 3 variables a, b, and c
	a = 5;
	b = 6;
	c = a + b; //You can do math
	cout << c << endl;

	int trump = 1000000000 + 5;
	trump = trump / 100000;
	cout << trump << endl;

	return 0;
}